﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffect : MonoBehaviour {

    string statusName = "";

    //please make all this private! and just use getters and setters

    public float damagePerTurn = 0f;
    public DamageType damageType = DamageType.fire;

    Animation statusEffectAnimation;
    public float maxHealthMod = 100.0f;
    public float spdMod = 100f;
    public float defMod = 100f;
    public float strMod = 100f;
    public float magMod = 100f;
    public float mdfMod = 100f;
    public float dexMod = 100f;
    public float lckMod = 100f;

    public int duration = 0;

    public bool clearsOnBattleEnd = true;

    public bool canStack = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
