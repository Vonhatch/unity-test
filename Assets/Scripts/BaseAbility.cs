﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetType { enemy, ally, self, mixed, random }

public class BaseAbility : MonoBehaviour {

    internal string abilityName;
    internal TargetType targetType;
    internal int maxTargets;

    public Animator animator;
}